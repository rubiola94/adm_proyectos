-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 14-12-2017 a las 00:12:07
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `mydb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE IF NOT EXISTS `persona` (
  `idpersona` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `dni` int(11) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `tipo_persona_idtipo_persona` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpersona`),
  KEY `fk_persona_tipo_persona1_idx` (`tipo_persona_idtipo_persona`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idpersona`, `nombre`, `dni`, `telefono`, `tipo_persona_idtipo_persona`) VALUES
(1, 'lourdes', 434234, 345, 2),
(2, 'oscar', 3423, 3242, 1),
(3, 'romina', 24234, 324234, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE IF NOT EXISTS `proyecto` (
  `idproyecto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_proyecto` varchar(45) DEFAULT NULL,
  `descripcion_proyecto` varchar(45) DEFAULT NULL,
  `fecha_inicio_proyecto` date DEFAULT NULL,
  `fecha_fin_proyecto` date DEFAULT NULL,
  `estado_proyecto` int(11) DEFAULT NULL,
  `avance_proyecto` int(11) DEFAULT NULL,
  `persona_idpersona` int(11) NOT NULL,
  PRIMARY KEY (`idproyecto`),
  KEY `fk_proyecto_persona1_idx` (`persona_idpersona`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`idproyecto`, `nombre_proyecto`, `descripcion_proyecto`, `fecha_inicio_proyecto`, `fecha_fin_proyecto`, `estado_proyecto`, `avance_proyecto`, `persona_idpersona`) VALUES
(1, 'pro1', 'probando', '2017-11-12', '2017-12-11', 1, 1, 1),
(2, 'pro2', 'dljafÃ±sh', '2017-12-12', '2017-12-26', 1, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recurso_material`
--

CREATE TABLE IF NOT EXISTS `recurso_material` (
  `idrecurso_material` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_recurso` varchar(100) DEFAULT NULL,
  `cantidad_necesaria` varchar(45) DEFAULT NULL,
  `habilitacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idrecurso_material`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `recurso_material`
--

INSERT INTO `recurso_material` (`idrecurso_material`, `descripcion_recurso`, `cantidad_necesaria`, `habilitacion`) VALUES
(1, 'impresora', '2', 1),
(2, 'monitor', '3', 1),
(3, 'cpu', '4', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recurso_material_has_tarea`
--

CREATE TABLE IF NOT EXISTS `recurso_material_has_tarea` (
  `recurso_material_idrecurso_material` int(11) NOT NULL,
  `tarea_idtarea` int(11) NOT NULL,
  `idrecursomaterial_has_tarea` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idrecursomaterial_has_tarea`),
  KEY `fk_recurso_material_has_tarea_tarea1_idx` (`tarea_idtarea`),
  KEY `fk_recurso_material_has_tarea_recurso_material1_idx` (`recurso_material_idrecurso_material`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `recurso_material_has_tarea`
--

INSERT INTO `recurso_material_has_tarea` (`recurso_material_idrecurso_material`, `tarea_idtarea`, `idrecursomaterial_has_tarea`) VALUES
(1, 1, 2),
(1, 3, 4),
(2, 4, 6),
(3, 4, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_usuario`
--

CREATE TABLE IF NOT EXISTS `rol_usuario` (
  `idrol_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_rol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idrol_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `rol_usuario`
--

INSERT INTO `rol_usuario` (`idrol_usuario`, `descripcion_rol`) VALUES
(1, 'analista');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea`
--

CREATE TABLE IF NOT EXISTS `tarea` (
  `idtarea` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_tarea` varchar(45) DEFAULT NULL,
  `descripcion_tarea` varchar(45) DEFAULT NULL,
  `cantidad_personas` varchar(45) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `prioridad` int(11) DEFAULT NULL,
  `estado_tarea` int(11) DEFAULT NULL,
  `costo_tarea` float DEFAULT NULL,
  `proyecto_idproyecto` int(11) NOT NULL,
  PRIMARY KEY (`idtarea`),
  KEY `fk_tareas_proyecto_idx` (`proyecto_idproyecto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `tarea`
--

INSERT INTO `tarea` (`idtarea`, `nombre_tarea`, `descripcion_tarea`, `cantidad_personas`, `fecha_inicio`, `fecha_fin`, `prioridad`, `estado_tarea`, `costo_tarea`, `proyecto_idproyecto`) VALUES
(1, 'tarea1', 'fasdf', '3', '2017-12-14', '2017-12-20', 2, 1, 50, 1),
(2, 'tarea2', 'fasdf', '3', '2017-12-14', '2017-12-16', 2, 1, 70, 1),
(3, 'tarea 3', 'ddfasfd', '3', '2017-12-07', '2017-12-13', 1, 1, 50, 1),
(4, 'tarea1', 'sdfasdf', '1', '2017-12-12', '2017-12-20', 2, 1, 45, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea_has_persona`
--

CREATE TABLE IF NOT EXISTS `tarea_has_persona` (
  `tarea_idtarea` int(11) NOT NULL,
  `persona_idpersona` int(11) NOT NULL,
  `idtarea_has_persona` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idtarea_has_persona`),
  KEY `fk_tarea_has_persona_persona1_idx` (`persona_idpersona`),
  KEY `fk_tarea_has_persona_tarea1_idx` (`tarea_idtarea`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `tarea_has_persona`
--

INSERT INTO `tarea_has_persona` (`tarea_idtarea`, `persona_idpersona`, `idtarea_has_persona`) VALUES
(1, 2, 1),
(1, 2, 2),
(1, 1, 3),
(2, 2, 4),
(2, 2, 5),
(2, 1, 6),
(3, 1, 7),
(3, 2, 8),
(3, 1, 9),
(4, 2, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_persona`
--

CREATE TABLE IF NOT EXISTS `tipo_persona` (
  `idtipo_persona` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipo_persona`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tipo_persona`
--

INSERT INTO `tipo_persona` (`idtipo_persona`, `descripcion`) VALUES
(1, 'rrhh'),
(2, 'responsable');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_usuario` varchar(45) NOT NULL,
  `contrasenia` varchar(45) NOT NULL,
  `tipo_usuario` int(11) DEFAULT NULL,
  `estado_usuario` int(11) DEFAULT NULL,
  `persona_idpersona` int(11) NOT NULL,
  `rol_usuario_idrol_usuario` int(11) NOT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `fk_usuario_rol_usuario1_idx` (`rol_usuario_idrol_usuario`),
  KEY `fk_usuario_persona1_idx` (`persona_idpersona`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='					\n\n' AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre_usuario`, `contrasenia`, `tipo_usuario`, `estado_usuario`, `persona_idpersona`, `rol_usuario_idrol_usuario`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 1, 1, 1),
(3, 'romina', '21232f297a57a5a743894a0e4a801fc3', 2, 1, 3, 1),
(4, 'oscar', '21232f297a57a5a743894a0e4a801fc3', 3, 1, 1, 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `fk_persona_tipo_persona1` FOREIGN KEY (`tipo_persona_idtipo_persona`) REFERENCES `tipo_persona` (`idtipo_persona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `fk_proyecto_persona1` FOREIGN KEY (`persona_idpersona`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `recurso_material_has_tarea`
--
ALTER TABLE `recurso_material_has_tarea`
  ADD CONSTRAINT `fk_recurso_material_has_tarea_recurso_material1` FOREIGN KEY (`recurso_material_idrecurso_material`) REFERENCES `recurso_material` (`idrecurso_material`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_recurso_material_has_tarea_tarea1` FOREIGN KEY (`tarea_idtarea`) REFERENCES `tarea` (`idtarea`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tarea`
--
ALTER TABLE `tarea`
  ADD CONSTRAINT `fk_tareas_proyecto` FOREIGN KEY (`proyecto_idproyecto`) REFERENCES `proyecto` (`idproyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tarea_has_persona`
--
ALTER TABLE `tarea_has_persona`
  ADD CONSTRAINT `fk_tarea_has_persona_persona1` FOREIGN KEY (`persona_idpersona`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tarea_has_persona_tarea1` FOREIGN KEY (`tarea_idtarea`) REFERENCES `tarea` (`idtarea`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_rol_usuario1` FOREIGN KEY (`rol_usuario_idrol_usuario`) REFERENCES `rol_usuario` (`idrol_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
