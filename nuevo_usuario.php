<?php require_once('Connections/conexion_admin_proyectos.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php 
    mysql_select_db($database_conexion_proyectos, $conexion_admin_proyectos);
    

    /* NO ESTA BIEN POR QUE AL USAR COMO CONDICION QUE SEA DISTINTO DE  $row_consulta[persona_idpersona] ESTAS COMPARANDO SOLO CON EL ULTIMO USUARIO ENCONTRADO, NO CON TODOS LOS DE LA TABLA USUARIO TE FALTARIA UN WHILE PARA RECORRER TODOS 
    AHI TE CAMBIO LA CONSULTA Y CON ESO YA ESTARIA FUNCIONANDO BIEN*/
    /*
    $q_consultapersona="SELECT persona_idpersona FROM usuario";
    $consulta=mysql_query($q_consultapersona) or die(mysql_error());
    $row_consulta=mysql_fetch_array($consulta);
    $q_persona=mysql_query("SELECT idpersona,nombre FROM persona WHERE idpersona != $row_consulta[persona_idpersona]");
    $row_persona=mysql_fetch_array($q_persona);
    */
    $q_persona=mysql_query("SELECT idpersona,  nombre FROM persona LEFT JOIN usuario ON persona_idpersona=idpersona WHERE idpersona NOT IN (SELECT persona_idpersona FROM usuario)") or die(mysql_error());
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include "sis_header.php" ?>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <?php include "sys_menu_vertical.php" ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">

                            Usuario <small>(Crear nuevo usuario)</small>
                        </h1>
                        <!-- NAVEGADOR -->
                        <ol class="breadcrumb">
                            <li>
                                <a href="principal.php"><i class="fa fa-home" aria-hidden="true"></i> Principal</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-fw fa-user"></i> Usuario nuevo
                            </li>
                        </ol>
                        <!-- FIN NAVEGADOR -->
                    </div>
                </div>
                <!-- /.row -->
                <div id="resultado" class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nombre de usuario</label>
                            <input id="nombreusuario" type="text" class="form-control" placeholder="Ingrese nombre de usuario" required>
                        </div>
                    <div class="form-group">
                                <label>Contraseña</label>
                                <input id="pass" type="password" class="form-control" placeholder="Ingrese contraseña" required>
                        </div> 
                            <!-- MENSAJE DE ALERTA PARA ALGUNA NOTIFICACION -->
                        <div class="form-group">
                            <label>Repetir contraseña</label>
                            <input id="pass2" type="password" class="form-control" placeholder="Ingrese contraseña nuevamente" required>
                        </div>
                            <!-- ALERTA SI LAS CONTRASEÑAS NO COINCIDEN (COMPROBACION CON JQUERY) -->

                        <div class="row" style="display:none;" id="alerta">
                            <div class="col-lg-12">
                                <div class="alert alert-danger alert-dismissable" align="center">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <i class="fa fa-times fa-2x" aria-hidden="true"></i>
                                    <br>
                                    <strong>Error!</strong> Las Contraseñas no coniciden.
                                </div>
                            </div>
                        </div>

                        <!-- /.row alert -->
                        <!-- ALERTA SI LAS CONTRASEÑAS COINCIDEN (COMPROBACION CON JQUERY) -->

                        <div class="row" style="display:none;" id="alerta_succes">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissable" align="center">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <i class="fa fa-check fa-2x" aria-hidden="true"></i>
                                    <br>
                                    <strong></strong> Las Contraseñas coniciden.
                                </div>
                            </div>
                        </div>
                        <!-- /.row alert -->
                        <div id="selectpersona" class="form-group" ">
                        <label>Persona:</label>
                        <select id="idpersona" class="form-control">
                        <option>Seleccione la Persona</option>
                        <?php 
                        while ($row_persona=mysql_fetch_array($q_persona)) { 
                            ?>
                             <option value="<?php echo $row_persona['idpersona'] ?>"><?php echo $row_persona['nombre'] ?></option>
                         <?php } ?>
                        </select>
                    </div>
                        <div class="form-group">
                            <label>Tipo de Usuario</label>
                             <select id="tipous" class="form-control">
                                <option selected value="1">Administrador</option>
                                <option value="2">Responsable</option>
                                <option value="3">Desarrollo</option>
                            </select>
                        </div>
                        <button id="reset" class="btn btn-default">Limpiar</button>
                        <button id="habilitar" class="btn btn-default pull-right" disabled>Aceptar</button>
                </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
    <script type="text/javascript">
            $('#pass').change(function(event) {
                $('#pass2').change(function(event) {
                    var pass=$('#pass').val();
                    var pass2=$('#pass2').val();
                    if (pass==pass2 && pass!='' && pass2!='') {
                        $('#alerta').hide('slow');
                        $('#alerta_succes').show('slow');
                        $('#habilitar').removeAttr('disabled');
                    }
                    else{
                        $('#alerta').show('slow');
                        $('#alerta_succes').hide('slow'); 
                        $('#pass').val('');
                        $('#pass2').val('');
                        $('#habilitar').attr('disabled', true);
                    };
                });
            });
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
            $('#habilitar').click(function(event) {
                var nombre = $('#nombreusuario').val();
                var tipous = $('#tipous').val();
                var pass = $('#pass').val();
                var idpersona=$('#idpersona').val();
                $.ajax({
                    type: "GET",
                    url: "cargarusuario.php",
                    data: {"tipous" : tipous, "pass":pass, "nombre":nombre, "idpersona": idpersona},
                    dataType: "html",
                    beforeSend: function(){
                          //imagen de carga
                          $("#resultado").html("<p align='center'><img src='images/ajax-loader.gif' /></p>");
                    },
                    error: function(){
                          alert("error petición ajax");
                    },
                    success: function(data){                                                    
                          $("#resultado").empty();
                          $("#resultado").append(data);
                                                             
                    }
              });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#principal').removeAttr('class');
            $('#usuario').attr('class', 'active');    
        });
    </script>
    <script type="text/javascript">
    $('#reset').click(function(event) {
        $('#nombreusuario').val('');
        $('#pass').val('');
        $('#pass2').val('');
        $('#tipous').val('0');
        $('#alerta').hide('slow');
        $('#alerta_succes').hide('slow');
        
    });
    </script>

</body>

</html>