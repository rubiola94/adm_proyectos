<?php require_once('Connections/conexion_admin_proyectos.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include "sis_header.php" ?>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <?php include "sys_menu_vertical.php" ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">

                            Personal <small>(Crear nuevo Personal)</small>
                        </h1>
                        <!-- NAVEGADOR -->
                        <ol class="breadcrumb">
                            <li>
                                <a href="principal.php"><i class="fa fa-home" aria-hidden="true"></i> Principal</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-fw fa-user"></i> Personal nuevo
                            </li>
                        </ol>
                        <!-- FIN NAVEGADOR -->
                    </div>
                </div>
                <!-- /.row -->
                <div id="resultado" class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nombre del Personal</label>
                            <input id="nombre" type="text" class="form-control" placeholder="Ingrese del nuevo personal" required>
                        </div>
                        <div class="form-group">
                            <label>DNI</label>
                            <input id="dni" type="text" class="form-control" placeholder="Ingrese el dni" required>
                        </div>
                        <div class="form-group">
                            <label>Telefono</label>
                            <input id="telefono" type="text" class="form-control" placeholder="Ingrese el telefono" required>
                        </div>
                   
                        <div class="form-group">
                            <label>Tipo de Personal</label>
                             <select id="idtipo_persona" class="form-control">
                                <option value="1">Recurso Humano</option>
                                <option selected value="2">Responsable</option>
                            </select>
                        </div>
                        <button id="reset" class="btn btn-default">Limpiar</button>
                        <button id="habilitar" class="btn btn-default pull-right" >Aceptar</button>
                </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
 

    <script type="text/javascript">
    $(document).ready(function() {
            $('#habilitar').click(function(event) {
                var nombre = $('#nombre').val();
                var dni = $('#dni').val();
                var telefono = $('#telefono').val();
                var idtipo_persona= $('#idtipo_persona').val();
                $.ajax({
                    type: "GET",
                    url: "cargapersonal.php",
                    data: {"nombre" : nombre, "dni":dni, "telefono":telefono,"idtipo_persona": idtipo_persona},
                    dataType: "html",
                    beforeSend: function(){
                          //imagen de carga
                          $("#resultado").html("<p align='center'><img src='images/ajax-loader.gif' /></p>");
                    },
                    error: function(){
                          alert("error petición ajax");
                    },
                    success: function(data){                                                    
                          $("#resultado").empty();
                          $("#resultado").append(data);
                                                             
                    }
              });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#principal').removeAttr('class');
            $('#personal').attr('class', 'active');    
        });
    </script>
    <script type="text/javascript">
    $('#reset').click(function(event) {
        $('#nombre').val('');
        $('#dni').val('');
        $('#telefono').val('');       
    });
    </script>

</body>

</html>