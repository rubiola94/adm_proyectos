<?php require_once('Connections/conexion_admin_proyectos.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
<?php include "sis_header.php" ?>
</head>

<body style="background-color: white">

    <div id="wrapper">
        <!-- Navigation -->
        
        <?php include "sys_menu_vertical.php" ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Diagrama de Gantt
                        </h1>
                        <ol class="breadcrumb">
                        <a href="principal.php">
                            <li class="active">
                                <i class="fa fa-home"></i> Diagrama
                            </li>
                        </a>
                        </ol>
                    </div>
                </div>
                <!-- /.row  -->
            <?php if ($_SESSION["tipousuario"]==1) { ?>
				<?php include "consulta_gantt.php" ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div id="container"></div>
                    </div>
                </div>
            <?php }?>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

</body>

</html>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/xrange.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'xrange'
    },
    title: {
        text: 'Diagrama de Gantt'
    },
    xAxis: {
        type: 'datetime'
    },
    yAxis: {
        title: {
            text: 'Tareas'
        },
        categories: [<?php echo $categorias; ?>],
        reversed: true
    },
    series: [{
        name: '<?php echo $nombre_proyecto; ?>',
        // pointPadding: 0,
        // groupPadding: 0,
        borderColor: 'gray',
        pointWidth: 20,
        data: [<?php echo $data; ?>],
        dataLabels: {
            enabled: false
        }
    }]

});
</script>