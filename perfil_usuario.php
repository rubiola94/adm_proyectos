<?php require_once('Connections/conexion_admin_proyectos.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include "sis_header.php" ?>
</head>
<body style="background-color: white;">
    <div id="wrapper">
        <!-- Navigation -->
        <?php include "sys_menu_vertical.php" ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Usuario
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="principal.php"><i class="fa fa-home" aria-hidden="true"></i> Principal</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-fw fa-user"></i> Usuario
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div id="resultado" class="row">
                    <div class="col-lg-6">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <img src="images/login/user_avatar_1.png" class="img-responsive" alt="Image">
                        </div>
                        <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                <div id="ocultar_nombre">
                                <label>Nombre de Usuario</label>
                                <input class="form-control" readonly value="<?php echo $_SESSION['usuarioactual'] ?>">
                                </div>
                                <p class="help-block">
                                <a href="#cambio_datos">
                                    <div id="mostrar_cambio"><i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                   Cambiar Contraseña / Nombre de Usuario
                                   </div>
                                </a>
                                </p>
                                   
                        </div>
                        <div class="clearfix"></div>

                        <div id="cambio_datos" style="display:none;">
                            <div class="form-group">
                                <label>Nombre de Usuario</label>
                                <input class="form-control" id="nombreusuario" value="<?php echo $_SESSION['usuarioactual'] ?>" required>
                                <p class="help-block">Ingrese un nombre de usuario nuevo.</p>
                            </div>

                            <div class="form-group">
                                <label>Contraseña Actual</label>
                                <input class="form-control" id="pass_actual" type="password" placeholder="Ingrese la contraseña actual" required>
                            </div>

                            <div class="form-group">
                                <label>Contraseña Nueva</label>
                                <input class="form-control" id="pass_nuevo" type="password" placeholder="Ingrese la nueva contraseña" required>
                            </div>

                            <div class="form-group">
                                <label>Repetir Contraseña Nueva</label>
                                <input class="form-control" id="pass_nuevo_conf" type="password" placeholder="Repita la nueva contraseña" required>
                            </div> 
                                <!-- ALERTA SI LAS CONTRASEÑAS NO COINCIDEN (COMPROBACION CON JQUERY) -->

                        <div class="row" style="display:none;" id="alerta">
                            <div class="col-lg-12">
                                <div class="alert alert-danger alert-dismissable" align="center">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <i class="fa fa-times fa-2x" aria-hidden="true"></i>
                                    <br>
                                    <strong>Error!</strong> Las Contraseñas no coniciden.
                                </div>
                            </div>
                        </div>

                        <!-- /.row alert -->
                        <!-- ALERTA SI LAS CONTRASEÑAS COINCIDEN (COMPROBACION CON JQUERY) -->

                        <div class="row" style="display:none;" id="alerta_succes">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissable" align="center">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <i class="fa fa-check fa-2x" aria-hidden="true"></i>
                                    <br>
                                    <strong></strong> Las Contraseñas coniciden.
                                </div>
                            </div>
                        </div>
                        <!-- /.row alert -->
                            <button type="reset" class="btn btn-default pull-left">Limpiar</button>  
                            <button id="habilitar" class="btn btn-default pull-right" disabled>Modificar</button>
                        </div>


                        <!-- FIN FORMULARIO -->
                        <br><br>
                        <br><br>
                    
                        
                    </div>
                        
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    
    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
        $('#mostrar_cambio').on('click', function(event) {
            $('#cambio_datos').toggle('slow');
            $('#ocultar_nombre').toggle('slow');
        });
    });
    </script>
    <script type="text/javascript">
        $('#pass_nuevo').change(function(event) {
            $('#pass_nuevo_conf').change(function(event) {
                var pass=$('#pass_nuevo').val();
                var pass2=$('#pass_nuevo_conf').val();
                if (pass==pass2 && pass!='' && pass2!='') {
                    $('#alerta').hide('slow');
                    $('#alerta_succes').show('slow');
                    $('#habilitar').removeAttr('disabled');
                }
                else{
                    $('#alerta').show('slow');
                    $('#alerta_succes').hide('slow'); 
                    $('#pass_nuevo').val('');
                    $('#pass_nuevo_conf').val('');
                    $('#habilitar').attr('disabled', true);
                    };
            });
        });
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
            $('#habilitar').click(function(event) {
                var pass_nuevo = $('#pass_nuevo').val();
                var pass_nuevo_conf = $('#pass_nuevo_conf').val();
                var pass_actual = $('#pass_actual').val();
                var nombreusuario = $('#nombreusuario').val();
                $.ajax({
                    type: "POST",
                    url: "perfil_usuario_modificacion.php",
                    data: {"pass_actual" : pass_actual, "pass_nuevo":pass_nuevo, "pass_nuevo_conf":pass_nuevo_conf, "nombreusuario":nombreusuario},
                    dataType: "html",
                    beforeSend: function(){
                          //imagen de carga
                          $("#resultado").html("<p align='center'><img src='images/ajax-loader.gif' /></p>");
                    },
                    error: function(){
                          alert("error petición ajax");
                    },
                    success: function(data){                                                    
                          $("#resultado").empty();
                          $("#resultado").append(data);
                                                             
                    }
              });
            });
        });
    </script>

</body>

</html>
