<?php require_once('Connections/conexion_admin_proyectos.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include "sis_header.php" ?>
<title>Administraci&oacute;n de Recursos</title>
</head>

<body>

<div class="dialog col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
    <div class="panel panel-default">
        <p class="panel-heading no-collapse">Administraci&oacute;n de Proyectos</p>
        <div class="panel-body">
            <form ACTION="sis_acceso_control.php" METHOD="POST" id="conecta">
                <div class="form-group">
                  <label>Usuario</label>
                    <input name="nombreusuario" type="text" class="form-control span12" id="nombreusuario">
                </div>
                <div class="form-group">
                <label>Contraseña</label>
                    <input name="pass" type="password" class="form-controlspan12 form-control" id="pass">
                </div>
                 <input type="submit" name="button" id="button" value="Enviar" class="btn btn-primary pull-right" />
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
