<?php require_once('Connections/conexion_admin_proyectos.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php
    mysql_select_db($database_conexion_proyectos, $conexion_admin_proyectos);
    $query =mysql_query("SELECT * FROM proyecto WHERE estado_proyecto=1 OR estado_proyecto=0") or die(mysql_error());
?>
<!DOCTYPE html>
<html lang="en">

<head>
<?php include "sis_header.php" ?>
</head>

<body style="background-color: white">

    <div id="wrapper">
        <!-- Navigation -->
        
        <?php include "sys_menu_vertical.php" ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Nuevo tarea
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-table"></i><a href="proyectos.php"> Proyectos</a>
                            </li>
                            <li class="active">
                                    Seleccionar proyecto
                            </li>
                        </ol>
                    </div>
                </div>

                <!-- ---------------------------------------------Formulario------------------------------------------------------- -->
                <div id="resultado" class="row"> 
                    <form action="tarea_alta.php" method="GET" role="form">
                         <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
                            <label>Seleccionar proyecto</label>
                            <select id="seleccionar_proyecto" name="idproyecto" class="form-control">
                                <option value="0">Seleccione un proyecto</option>
                                <?php 
                                    while ($row_query=mysql_fetch_array($query)) { ?>
                                        <option value="<?php echo $row_query['idproyecto'] ?>"><?php echo $row_query['nombre_proyecto'] ?></option>
                                   <?php }
                                ?>
                            </select>
                        </div>
                        <div class="clearfix">
                        </div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                            <button id="habilitar" type="submit" class="btn btn-default pull-right" disabled title="Debe seleccionar un proyecto para avanzar">Siguiente</button>
                        </div>
                    </form> 
                </div>
          <!-- ---------------------------------------------------fin-------------------------------------------------------------- -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#principal').removeAttr('class');
            $('#proyecto').attr('class', 'active');    
        });
    </script>

    <script type="text/javascript">
    $('#seleccionar_proyecto').change(function(event) {
        var idproyecto = $('#seleccionar_proyecto').val();
        if (idproyecto!=0) {
            $('#habilitar').removeAttr('disabled');
        }
        else{
            $('#habilitar').attr('disabled', true);;
        }
        ;
    });
    </script>

</body>

</html>